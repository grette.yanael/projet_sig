package fr.projet.sig.m2info.model;


import java.util.Date;

public class Alerteveloplus {

    private Long idalerte;


    private Stationveloplus stationveloplus;

    private String titrealerte;


    private String type; //détérioration, retard, coupure

    private String textalert;

    private Date date;
    private int cptresolu;


    public Alerteveloplus() {}

    public Long getIdalerte() {
        return idalerte;
    }

    public void setIdalerte(Long idalerte) {
        this.idalerte = idalerte;
    }

    public Stationveloplus getStationveloplus() {
        return stationveloplus;
    }

    public void setStationveloplus(Stationveloplus stationveloplus) {
        this.stationveloplus = stationveloplus;
    }

    public String getTitrealerte() {
        return titrealerte;
    }

    public void setTitrealerte(String titrealerte) {
        this.titrealerte = titrealerte;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTextalert() {
        return textalert;
    }

    public void setTextalert(String textalert) {
        this.textalert = textalert;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCptresolu() {
        return cptresolu;
    }

    public void setCptresolu(int cptresolu) {
        this.cptresolu = cptresolu;
    }
}
