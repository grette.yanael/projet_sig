package fr.projet.sig.m2info.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.adapter.AdapterAlerteDetails;
import fr.projet.sig.m2info.adapter.AdapterArret;
import fr.projet.sig.m2info.adapter.AdapterLigne;
import fr.projet.sig.m2info.item.AlerteDetailsItem;
import fr.projet.sig.m2info.item.ArretItem;
import fr.projet.sig.m2info.item.LigneItem;
import fr.projet.sig.m2info.model.Alerte;
import fr.projet.sig.m2info.model.Alertearret;
import fr.projet.sig.m2info.model.Arret;
import fr.projet.sig.m2info.model.Ligne;

public class ActivityArret extends AppCompatActivity {

    AsyncTask<Object, Void, Void> inBackground;
    AlertDialog dialog;
    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_un_arret);
        inBackground = new RunInBackground();
        inBackground.execute();
    }


    private class RunInBackground extends AsyncTask<Object, Void, Void> {

        private Arret arret;

        private String readStream(InputStream is) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }
        @Override
        protected Void doInBackground(Object... objects) {
            SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
            String ip = sharedPreferences.getString("ip","");
            Intent i = getIntent();
            long id = i.getLongExtra("idArret",-1);
            String urlString = "http://"+ip+":8081/transport/arret?idArret="+id;
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();
            String result = getResult(urlString);
            if(result == null)
                return null;
            arret = gson.fromJson(result, Arret.class);
            return null;
        }

        @Override
        protected  void onPostExecute(Void unused) {
            if(arret == null)
                return;
            TextView tv_nom_arret = findViewById(R.id.tv_nom_arret);
            tv_nom_arret.setText(arret.getNomarret());
            RecyclerView rv = findViewById(R.id.rv_correspondance);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
            rv.setLayoutManager(layoutManager);
            ArrayList<LigneItem> ligneItems = new ArrayList<>();
            for(Ligne l:arret.getLignes()) {
                ligneItems.add(new LigneItem(l));
            }
            AdapterLigne adapter = new AdapterLigne(ligneItems);
            rv.setAdapter(adapter);




            Button btn_alerte = findViewById(R.id.btn_alerte);
            ArrayList<AlerteDetailsItem> items = new ArrayList<>();
            for (Alertearret aa : arret.getAlertearrets())
                items.add(new AlerteDetailsItem(aa.getDate(), aa.getTextalert()));
            btn_alerte.setText(MessageFormat.format("{0}", items.size()));
            if(items.size()> 0) {
                AdapterAlerteDetails adapterAlerte = new AdapterAlerteDetails(items);
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityArret.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.popup_alerte, null);
                TextView tv_info = dialogView.findViewById(R.id.tv_info);
                tv_info.setText(arret.getNomarret());
                RecyclerView rv_alertes = dialogView.findViewById(R.id.rv_details);
                layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                rv_alertes.setLayoutManager(layoutManager);
                rv_alertes.setAdapter(adapterAlerte);
                builder.setCancelable(true);
                builder.setView(dialogView);
                dialog = builder.create();
                btn_alerte.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.show();
                    }
                });
            }
            else
                btn_alerte.setVisibility(View.GONE);

            findViewById(R.id.tv_creer_alerte).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ActivityNewAlerte.class);
                    intent.putExtra("idRadio",1);
                    intent.putExtra("id",arret.getIdarret());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                }
            });

        }

        private String getResult(String urlString) {
            String result = null;
            try {
                URL url = new URL(urlString);
                HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(httpConnection .getInputStream()); // Stream
                result = readStream(in); // Read stream
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }


}
