package fr.projet.sig.m2info.model;


import java.util.Date;

public class Alertearret {

    private Long idalerte;
    private Arret arret;
    private String titrealerte;
    private String type; //détérioration, retard, coupure
    private String textalert;
    private Date date;
    private int cptresolu;


    public Alertearret() {}

    public Long getIdalerte() {
        return idalerte;
    }

    public void setIdalerte(Long idalerte) {
        this.idalerte = idalerte;
    }

    public Arret getIdarret() {
        return arret;
    }

    public void setIdarret(Arret arret) {
        this.arret = arret;
    }

    public String getTitrealerte() {
        return titrealerte;
    }

    public void setTitrealerte(String titrealerte) {
        this.titrealerte = titrealerte;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTextalert() {
        return textalert;
    }

    public void setTextalert(String textalert) {
        this.textalert = textalert;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCptresolu() {
        return cptresolu;
    }

    public void setCptresolu(int cptresolu) {
        this.cptresolu = cptresolu;
    }

    public Arret getArret() {
        return arret;
    }

    public void setArret(Arret arret) {
        this.arret = arret;
    }


}
