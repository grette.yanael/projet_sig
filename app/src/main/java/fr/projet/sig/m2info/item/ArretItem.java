package fr.projet.sig.m2info.item;

import android.os.Parcel;
import android.os.Parcelable;

import fr.projet.sig.m2info.model.Arret;

public class ArretItem implements Parcelable {
    private Long id;
    private String nomArret;


    public ArretItem(Arret arret) {
        this.id = arret.getIdarret();
        this.nomArret = arret.getNomarret();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomArret() {
        return nomArret;
    }

    public void setNomArret(String nomArret) {
        this.nomArret = nomArret;
    }

    protected ArretItem(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        nomArret = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(nomArret);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ArretItem> CREATOR = new Creator<ArretItem>() {
        @Override
        public ArretItem createFromParcel(Parcel in) {
            return new ArretItem(in);
        }

        @Override
        public ArretItem[] newArray(int size) {
            return new ArretItem[size];
        }
    };
}
