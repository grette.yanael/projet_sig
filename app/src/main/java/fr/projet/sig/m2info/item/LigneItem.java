package fr.projet.sig.m2info.item;

import android.os.Parcel;
import android.os.Parcelable;

import fr.projet.sig.m2info.model.Ligne;

public class LigneItem implements Parcelable {
    private String numLigne;
    private String direction1;
    private String direction2;
    private String route_color;
    private String text_color;

    public LigneItem() {}


    public LigneItem(Ligne l) {
        this.numLigne = l.getNumligne();
        this.direction1 = l.getDirection1();
        this.direction2 = l.getDirection2();
        this. route_color  = l.getRoute_color();
        this.text_color = l.getText_color();
    }


    protected LigneItem(Parcel in) {
        numLigne = in.readString();
        direction1 = in.readString();
        direction2 = in.readString();
        route_color = in.readString();
        text_color = in.readString();
    }

    public String getNumLigne() {
        return numLigne;
    }

    public void setNumLigne(String numLigne) {
        this.numLigne = numLigne;
    }

    public String getDirection1() {
        return direction1;
    }

    public void setDirection1(String direction1) {
        this.direction1 = direction1;
    }

    public String getDirection2() {
        return direction2;
    }

    public void setDirection2(String direction2) {
        this.direction2 = direction2;
    }

    public String getRoute_color() {
        return route_color;
    }

    public void setRoute_color(String route_color) {
        this.route_color = route_color;
    }

    public String getText_color() {
        return text_color;
    }

    public void setText_color(String text_color) {
        this.text_color = text_color;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(numLigne);
        dest.writeString(direction1);
        dest.writeString(direction2);
        dest.writeString(route_color);
        dest.writeString(text_color);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LigneItem> CREATOR = new Creator<LigneItem>() {
        @Override
        public LigneItem createFromParcel(Parcel in) {
            return new LigneItem(in);
        }

        @Override
        public LigneItem[] newArray(int size) {
            return new LigneItem[size];
        }
    };
}
