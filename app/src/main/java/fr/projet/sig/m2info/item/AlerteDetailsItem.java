package fr.projet.sig.m2info.item;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class AlerteDetailsItem implements Parcelable {
    private Date date;
    private String details;

    public AlerteDetailsItem() {
    }

    public AlerteDetailsItem(Date date, String details) {
        this.date = date;
        this.details = details;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    protected AlerteDetailsItem(Parcel in) {
        details = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(details);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AlerteDetailsItem> CREATOR = new Creator<AlerteDetailsItem>() {
        @Override
        public AlerteDetailsItem createFromParcel(Parcel in) {
            return new AlerteDetailsItem(in);
        }

        @Override
        public AlerteDetailsItem[] newArray(int size) {
            return new AlerteDetailsItem[size];
        }
    };
}
