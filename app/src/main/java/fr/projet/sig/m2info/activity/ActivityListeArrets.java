package fr.projet.sig.m2info.activity;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.adapter.AdapterArret;
import fr.projet.sig.m2info.adapter.AdapterArretLigne;
import fr.projet.sig.m2info.adapter.AdapterLigne;
import fr.projet.sig.m2info.item.ArretItem;
import fr.projet.sig.m2info.item.LigneItem;
import fr.projet.sig.m2info.model.Arret;
import fr.projet.sig.m2info.item.ArretLigneItem;
import fr.projet.sig.m2info.model.Ligne;

public class ActivityListeArrets extends AppCompatActivity {

    AsyncTask<Object, Void, Void> inBackground;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_liste_arrets);
        inBackground = new RunInBackground();
        inBackground.execute();
    }


    private class RunInBackground extends AsyncTask<Object, Void, Void> {

        private ArrayList<Arret> arrets;


        private String readStream(InputStream is) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }
        @Override
        protected Void doInBackground(Object... objects) {
            SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
            String ip = sharedPreferences.getString("ip","");

            String urlString = "http://"+ip+":8081/transport/arrets";

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();


            String result = getResult(urlString);
            if(result == null)
                return null;
            Type type = new TypeToken<ArrayList<Arret>>(){}.getType();
            arrets = gson.fromJson(result, type);
            return null;
        }

        @Override
        protected  void onPostExecute(Void unused) {
            if(arrets == null)
                return;
            RecyclerView rv = findViewById(R.id.rv_arrets);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
            rv.setLayoutManager(layoutManager);
            ArrayList<ArretItem> arretItems = new ArrayList<>();
            for(Arret a:arrets) {
                arretItems.add(new ArretItem(a));
            }
            AdapterArret adapter = new AdapterArret (arretItems);
            rv.setAdapter(adapter);
        }

        private String getResult(String urlString) {
            String result = null;
            try {
                URL url = new URL(urlString);
                HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(httpConnection .getInputStream()); // Stream
                result = readStream(in); // Read stream
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }
    }

}
