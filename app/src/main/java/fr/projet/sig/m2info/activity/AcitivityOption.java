package fr.projet.sig.m2info.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;
import fr.projet.sig.m2info.R;

public class AcitivityOption extends AppCompatActivity {
    private EditText et_ip, et_name;

    private final Pattern IP_ADDRESS
            = Pattern.compile(
            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9]))");

    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_option);
        final SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
        final boolean firstCo = sharedPreferences.getBoolean("firstCo",true);
        et_ip = findViewById(R.id.et_IP);
        et_name = findViewById(R.id.et_pseudo);
        if(!firstCo) {
            et_name.setText(sharedPreferences.getString("name",""));
            et_name.setEnabled(false);
            et_ip.setText(sharedPreferences.getString("ip",""));
        }
        Button valider = findViewById(R.id.btn_valider);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ip = et_ip.getText().toString();
                String pseudo = et_name.getText().toString();
                boolean ok = true;
                if(pseudo.length() < 5) {
                    //message erreur
                    ok = false;
                }
                Matcher matcher = IP_ADDRESS.matcher(ip);
                if(!matcher.matches()) {
                    ok=false;
                    //error
                }
                if(ok) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ip", ip);
                    edit.putString("name", pseudo);
                    edit.apply();
                    if(firstCo) {
                        edit.putBoolean("firstCo", false);
                        edit.apply();
                        startActivity(new Intent(AcitivityOption.this, ActivityMenu.class));
                    }
                    finish();
                }
            }
        });
    }
}
