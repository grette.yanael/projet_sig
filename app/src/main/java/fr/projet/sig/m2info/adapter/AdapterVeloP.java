package fr.projet.sig.m2info.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.activity.ActivityArret;
import fr.projet.sig.m2info.activity.ActivityListeVelosP;
import fr.projet.sig.m2info.activity.ActivityVeloP;
import fr.projet.sig.m2info.item.ArretItem;
import fr.projet.sig.m2info.item.VeloPItem;

public class AdapterVeloP  extends RecyclerView.Adapter<AdapterVeloP.ViewHolder>{
    private final ActivityListeVelosP activity;
    private ArrayList<VeloPItem> items;
    private Context mContext;

    public AdapterVeloP(ArrayList<VeloPItem> items, ActivityListeVelosP activity) {
        this.items = items;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from((parent.getContext())).inflate(R.layout.item_arret_liste, parent, false);
        mContext = parent.getContext();
        return new AdapterVeloP.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final VeloPItem veloPItem = items.get(position);
        holder.nomArret.setText(veloPItem.getNomarret());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityVeloP.class);
                intent.putExtra("lon", activity.getLon());
                intent.putExtra("lat", activity.getLat());
                intent.putExtra("idVeloP", veloPItem.getIdstationplus());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView nomArret;

        public ViewHolder(View itemView) {
            super(itemView);
            this.cv = itemView.findViewById(R.id.cv);
            this.nomArret = itemView.findViewById(R.id.tv_nom_arret);
        }
    }
}
