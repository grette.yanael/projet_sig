package fr.projet.sig.m2info.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.activity.ActivityArret;
import fr.projet.sig.m2info.item.ArretItem;

public class AdapterArret extends RecyclerView.Adapter<AdapterArret.ViewHolder>{
    private ArrayList<ArretItem> items;
    private Context mContext;

    public AdapterArret(ArrayList<ArretItem> arretItems) {
        this.items = arretItems;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from((parent.getContext())).inflate(R.layout.item_arret_liste, parent, false);
        mContext = parent.getContext();
        return new AdapterArret.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ArretItem arretItem = items.get(position);
        holder.nomArret.setText(arretItem.getNomArret());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityArret.class);
                intent.putExtra("idArret", arretItem.getId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView nomArret;

        public ViewHolder(View itemView) {
            super(itemView);
            this.cv = itemView.findViewById(R.id.cv);
            this.nomArret = itemView.findViewById(R.id.tv_nom_arret);
        }
    }
}
