package fr.projet.sig.m2info.model;

public class Alerte {

    private int niveau;
    private String description;
    private int minuteRetard;

    public Alerte() {
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMinuteRetard() {
        return minuteRetard;
    }

    public void setMinuteRetard(int minuteRetard) {
        this.minuteRetard = minuteRetard;
    }
}
