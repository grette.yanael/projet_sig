package fr.projet.sig.m2info.item;

import android.os.Parcel;
import android.os.Parcelable;

public class AlerteItem implements Parcelable {

    private Long id;
    private String numLigne;
    private String arret;
    private int nbAlerte;
    private boolean estLigne;

    public AlerteItem(Long id, String arret, int nbAlerte, boolean estLigne) {
        this.id = id;
        this.arret = arret;
        this.nbAlerte = nbAlerte;
        this.estLigne = estLigne;
    }

    public AlerteItem(String numLigne, int nbAlerte, boolean estLigne) {
        this.numLigne = numLigne;
        this.nbAlerte = nbAlerte;
        this.estLigne = estLigne;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumLigne() {
        return numLigne;
    }

    public void setNumLigne(String numLigne) {
        this.numLigne = numLigne;
    }

    public String getArret() {
        return arret;
    }

    public void setArret(String arret) {
        this.arret = arret;
    }

    public int getNbAlerte() {
        return nbAlerte;
    }

    public void setNbAlerte(int nbAlerte) {
        this.nbAlerte = nbAlerte;
    }

    public boolean isEstLigne() {
        return estLigne;
    }

    public void setEstLigne(boolean estLigne) {
        this.estLigne = estLigne;
    }

    protected AlerteItem(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        numLigne = in.readString();
        arret = in.readString();
        nbAlerte = in.readInt();
        estLigne = in.readByte() != 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(numLigne);
        dest.writeString(arret);
        dest.writeInt(nbAlerte);
        dest.writeByte((byte) (estLigne ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AlerteItem> CREATOR = new Creator<AlerteItem>() {
        @Override
        public AlerteItem createFromParcel(Parcel in) {
            return new AlerteItem(in);
        }

        @Override
        public AlerteItem[] newArray(int size) {
            return new AlerteItem[size];
        }
    };
}
