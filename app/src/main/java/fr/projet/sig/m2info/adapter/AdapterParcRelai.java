package fr.projet.sig.m2info.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.activity.ActivityListeParcRelai;
import fr.projet.sig.m2info.activity.ActivityParcRelai;
import fr.projet.sig.m2info.item.ParcVeloItem;

public class AdapterParcRelai extends RecyclerView.Adapter<AdapterParcRelai.ViewHolder> {
    private final ActivityListeParcRelai activity;
    private ArrayList<ParcVeloItem> items;
    private Context mContext;

    public AdapterParcRelai(ArrayList<ParcVeloItem> items, ActivityListeParcRelai actitivy) {
        this.items = items;
        this.activity = actitivy;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from((parent.getContext())).inflate(R.layout.item_arret_liste, parent, false);
        mContext = parent.getContext();
        return new AdapterParcRelai.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ParcVeloItem parcVeloItem = items.get(position);
        holder.nomArret.setText(parcVeloItem.getNomarretparc());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ActivityParcRelai.class);
                intent.putExtra("lon", activity.getLon());
                intent.putExtra("lat", activity.getLat());
                intent.putExtra("idParc", parcVeloItem.getIdparcv());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView nomArret;

        public ViewHolder(View itemView) {
            super(itemView);
            this.cv = itemView.findViewById(R.id.cv);
            this.nomArret = itemView.findViewById(R.id.tv_nom_arret);
        }
    }
}
