package fr.projet.sig.m2info.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;

import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.adapter.AdapterAlerteDetails;
import fr.projet.sig.m2info.adapter.AdapterArretLigne;
import fr.projet.sig.m2info.item.AlerteDetailsItem;
import fr.projet.sig.m2info.model.Alerteligne;
import fr.projet.sig.m2info.model.Arret;
import fr.projet.sig.m2info.item.ArretLigneItem;
import fr.projet.sig.m2info.model.Ligne;

public class ActivityLigne extends AppCompatActivity implements LocationListener {

    AsyncTask<Object, Void, Void> inBackground;
    private AlertDialog dialog;

    protected LocationManager locationManager;
    protected LocationListener locationListener;
    double lat, lon;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_arrets_une_ligne);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        inBackground = new RunInBackground();
        inBackground.execute();
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        else
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    @Override
    public void onLocationChanged(Location location) {
        lon = location.getLongitude();
        lat = location.getLatitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class RunInBackground extends AsyncTask<Object, Void, Void> {
        private Ligne ligne;
        private ArrayList<Arret> arrets;

        private String readStream(InputStream is) throws IOException {
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }
        @Override
        protected Void doInBackground(Object... objects) {
            String numLigne = getIntent().getStringExtra("numLigne");
            SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
            String ip = sharedPreferences.getString("ip","");
            String urlStringLigne = "http://"+ip+":8081/transport/ligne?numLigne="+numLigne;
            String urlStringArrets = "http://"+ip+":8081/transport/ligne/arrets?numLigne="+numLigne;

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();
            ligne = null;
            arrets = null;


            String resultLigne = getResult(urlStringLigne);
            String resultArrets = getResult(urlStringArrets);
            if(resultLigne == null || resultArrets == null) {
                return null;
            }
            ligne = gson.fromJson(resultLigne, Ligne.class);
            Type type = new TypeToken<ArrayList<Arret>>(){}.getType();
            arrets = gson.fromJson(resultArrets, type);
            return null;
        }

        @Override
        protected  void onPostExecute(Void unused) {
            TextView tv_ligne = findViewById(R.id.tv_ligne);
            TextView tv_direction = findViewById(R.id.tv_direction);
            LinearLayout ll_head = findViewById(R.id.ll_head);
            if(ligne == null|| arrets == null) {
                tv_ligne.setText(R.string.arrets_404_1);
                Toast.makeText(getApplicationContext(),"Une erreur est survenue lors de la récupération des données, assurez vous d'avoir accès à internet.", Toast.LENGTH_LONG).show();
                return;
            }
            ll_head.setBackgroundColor(Color.parseColor("#"+ligne.getRoute_color()));
            tv_ligne.setText(ligne.getNumligne());
            String direction = ligne.getDirection1();
            if(!ligne.getDirection2().equals("_"))
                direction += "-"+ligne.getDirection2();
            tv_direction.setText(direction);
            String text_color = ligne.getText_color();
            if(text_color.equals("0"))
                text_color = "000000";
            tv_ligne.setTextColor(Color.parseColor("#"+text_color));
            tv_direction.setTextColor(Color.parseColor("#"+text_color));
            RecyclerView rv = findViewById(R.id.rv_arret);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
            rv.setLayoutManager(layoutManager);
            ArrayList<ArretLigneItem> arretsItem = new ArrayList<>();
            for(Arret arret: arrets){
                ArretLigneItem ai = new ArretLigneItem(arret, ligne);
                arretsItem.add(ai);
            }
            rv.setAdapter(new AdapterArretLigne(arretsItem, ActivityLigne.this));

            Button btn_alerte = findViewById(R.id.btn_alerte);
            ArrayList<AlerteDetailsItem> items = new ArrayList<>();
            for (Alerteligne al : ligne.getAlertelignes())
                items.add(new AlerteDetailsItem(al.getDate(), al.getTextalert()));
            btn_alerte.setText(MessageFormat.format("{0}", items.size()));
            if(items.size()> 0) {
                AdapterAlerteDetails adapterAlerte = new AdapterAlerteDetails(items);
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLigne.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.popup_alerte, null);
                TextView tv_info = dialogView.findViewById(R.id.tv_info);
                tv_info.setText(ligne.getNumligne());
                RecyclerView rv_alertes = dialogView.findViewById(R.id.rv_details);
                layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
                rv_alertes.setLayoutManager(layoutManager);
                rv_alertes.setAdapter(adapterAlerte);
                builder.setCancelable(true);
                builder.setView(dialogView);
                dialog = builder.create();
                btn_alerte.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.show();
                    }
                });
            }
            else
                btn_alerte.setVisibility(View.GONE);


            findViewById(R.id.tv_creer_alerte).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ActivityNewAlerte.class);
                    intent.putExtra("idRadio",0);
                    intent.putExtra("numLigne",ligne.getNumligne());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                }
            });
        }

        private String getResult(String urlString) {
            String result = null;
            try {
                URL url = new URL(urlString);
                HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(httpConnection .getInputStream()); // Stream
                result = readStream(in); // Read stream
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }


    }
}
