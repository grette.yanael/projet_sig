package fr.projet.sig.m2info.model;


import java.util.ArrayList;
import java.util.Collection;

public class Arret {

    private Long idarret;
    private String nomarret;

    private Collection<Alertearret> alertearrets;

    private Collection<Ligne> lignes;

    public Arret() {
        this.alertearrets=new ArrayList<Alertearret>();
        this.lignes=new ArrayList<Ligne>();
    }

    public Long getIdarret() {
        return idarret;
    }

    public void setIdarret(Long idarret) {
        this.idarret = idarret;
    }

    public String getNomarret() {
        return nomarret;
    }

    public void setNomarret(String nomarret) {
        this.nomarret = nomarret;
    }

    public Collection<Alertearret> getAlertearrets() {
        return alertearrets;
    }

    public void addAlertearret(Alertearret alertearret) {
        alertearret.setArret(this);
        this.alertearrets.add(alertearret);
    }
    public void setAlertearrets(Collection<Alertearret> alertearrets) {
        this.alertearrets = alertearrets;
    }

    public Collection<Ligne> getLignes() {
        return lignes;
    }

    public void setLignes(Collection<Ligne> lignes) {
        this.lignes = lignes;
    }

    public void addLigne(Ligne ligne) {
        ligne.addArret(this);
        this.lignes.add(ligne);
    }
}
