package fr.projet.sig.m2info.item;

import android.os.Parcel;
import android.os.Parcelable;

import fr.projet.sig.m2info.model.Stationveloplus;

public class VeloPItem implements Parcelable {
    private Long idstationplus;
    private String nomarret;

    public VeloPItem() {
    }

    public VeloPItem(Stationveloplus veloP) {
        idstationplus = veloP.getIdstationplus();
        nomarret = veloP.getNomarret();
    }

    protected VeloPItem(Parcel in) {
        if (in.readByte() == 0) {
            idstationplus = null;
        } else {
            idstationplus = in.readLong();
        }
        nomarret = in.readString();
    }

    public Long getIdstationplus() {
        return idstationplus;
    }

    public void setIdstationplus(Long idstationplus) {
        this.idstationplus = idstationplus;
    }

    public String getNomarret() {
        return nomarret;
    }

    public void setNomarret(String nomarret) {
        this.nomarret = nomarret;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (idstationplus == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(idstationplus);
        }
        dest.writeString(nomarret);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VeloPItem> CREATOR = new Creator<VeloPItem>() {
        @Override
        public VeloPItem createFromParcel(Parcel in) {
            return new VeloPItem(in);
        }

        @Override
        public VeloPItem[] newArray(int size) {
            return new VeloPItem[size];
        }
    };
}
