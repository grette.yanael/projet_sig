package fr.projet.sig.m2info.activity;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import fr.projet.sig.m2info.R;
import fr.projet.sig.m2info.adapter.AdapterLigne;
import fr.projet.sig.m2info.item.LigneItem;
import fr.projet.sig.m2info.model.Ligne;

public class ActivityListeBus extends AppCompatActivity {

    private AsyncTask<Object, Void, Void> inBackground;
    private View rootView;
    private SearchView searchView;
    private ArrayList<Ligne> lignes;
    AdapterLigne adapter;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_liste_transport);
        rootView = findViewById(R.id.root_layout);
        rootView.requestFocus();
        searchView = findViewById(R.id.search);
        inBackground = new RunInBackground();
        inBackground.execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
        searchView.setQuery("", false);
        rootView.requestFocus();
    }

    private class RunInBackground extends AsyncTask<Object, Void, Void> {




        private String readStream(InputStream is) throws IOException { 
            StringBuilder sb = new StringBuilder();
            BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
            for (String line = r.readLine(); line != null; line =r.readLine()){
                sb.append(line);
            }
            is.close();
            return sb.toString();
        }
        @Override
        protected Void doInBackground(Object... objects) {
            SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
            String ip = sharedPreferences.getString("ip","");

            String urlString = "http://"+ip+":8081/transport/lignes";

            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
                        public Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
                            return new Date(jsonElement.getAsJsonPrimitive().getAsLong());
                        }
                    })
                    .create();


            String result = getResult(urlString);
            if(result == null)
                return null;
            Type type = new TypeToken<ArrayList<Ligne>>(){}.getType();
            lignes = gson.fromJson(result, type);
            return null;
        }

        @Override
        protected  void onPostExecute(Void unused) {
            if(lignes == null) {
                return;
            }
            RecyclerView rv = findViewById(R.id.rv_arret);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
            rv.setLayoutManager(layoutManager);
            ArrayList<LigneItem> busItems = new ArrayList<>();
            for(Ligne l:lignes) {
                LigneItem ligneItem = new LigneItem(l);
                if(!l.getNumligne().equals("TA") && !l.getNumligne().equals("TB"))
                    busItems.add(ligneItem);
            }
            adapter = new AdapterLigne(busItems);
            rv.setAdapter(adapter);

        }

        private String getResult(String urlString) {
            String result = null;
            try {
                URL url = new URL(urlString);
                HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(httpConnection .getInputStream()); // Stream
                result = readStream(in); // Read stream
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }


    }
}
