package fr.projet.sig.m2info.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.webkit.WebView;

import java.text.MessageFormat;

import javax.net.ssl.HttpsURLConnection;

import androidx.appcompat.app.AppCompatActivity;
import fr.projet.sig.m2info.R;

public class ActivityMap extends AppCompatActivity {
    String url;
    final String ousuisje = "ou-suis?long={0}&lat={1}";//id=0
    final String lignes = "lignes?long={0}&lat={1}";//id=1
    final String cyclable = "cyclable?long={0}&lat={1}";//id=2
    final String arret = "arret?numLigne={0}&idArret={1}&direction={2}&long={3}&lat={4}";//id=3


    @Override
    public void onCreate(Bundle onSavedInstance) {
        super.onCreate(onSavedInstance);
        setContentView(R.layout.activity_map);
        SharedPreferences sharedPreferences = getSharedPreferences("sig", MODE_PRIVATE);
        String ip = sharedPreferences.getString("ip","");
        url = "http://"+ip+":8081/map/";
        Intent intent = getIntent();
        int id = intent.getIntExtra("id",-1);
        double lon = intent.getDoubleExtra("long",-1);
        double lat = intent.getDoubleExtra("lat",-1);
        switch (id) {
            case 0:
                url += MessageFormat.format(ousuisje,lon, lat);
                break;
            case 2:
                url += MessageFormat.format(lignes,lon, lat);
                break;
            case 1:
                url += MessageFormat.format(cyclable,lon, lat);
                break;
            case 3:
                String numLigne = intent.getStringExtra("numLigne");
                String direction = intent.getStringExtra("direction").replace(" ","%20");
                Long idArret = intent.getLongExtra("idArret",-1);
                url += MessageFormat.format(arret, numLigne, idArret, direction, lon, lat);
                break;
        }
        WebView webView = findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.loadUrl(url);
    }

}
